package minio

import "log"
import "bytes"
import "crypto/sha1"
import "crypto/md5"
import "encoding/hex"
import m "github.com/minio/minio-go"

type Minio struct {
	bucketName string
	conn *m.Client
}

func hErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func Connect(endpoint string, accessKeyID string, secretAccessKey string, location string, bucketName string, useSSL bool) (*Minio) {
	min, err := m.New(endpoint, accessKeyID, secretAccessKey, useSSL)
	if err != nil {
		log.Fatalln(err)
	}
	exists, err := min.BucketExists(bucketName)
	if err == nil && exists {
		log.Printf("We already own %s\n", bucketName)
	} else {
		err = min.MakeBucket(bucketName, location)
		if err != nil {
			panic(err)
		}
	}
	log.Printf("Successfully selected %s\n", bucketName)
	self := Minio{
		conn: min,
		bucketName: bucketName,
	}
	return &self
}

func(self *Minio) Write(body []byte) string {
	id := calcId(body)
	object := bytes.NewReader(body)
	n, err := self.conn.PutObject(self.bucketName, id, object, object.Size(), m.PutObjectOptions{
		ContentType: "application/octet-stream",
	})
	hErr(err)
	log.Printf("Uploaded", id, " of size: ", n, "Successfully.")
	return id
}

func(self *Minio) Read(id string) ([]byte) {
	reader, err := self.conn.GetObject(self.bucketName, id, m.GetObjectOptions{})
	hErr(err)
	defer reader.Close()
	buf := new(bytes.Buffer)
	buf.ReadFrom(reader)
	body := buf.Bytes()
	log.Printf("Downloaded", id, "Successfully.")
	return body
}

func calcId(body []byte) (string) {
	sha := sha1.New()
	sha.Write(body)
	a := hex.EncodeToString(sha.Sum(nil))
	md := md5.New()
	md.Write(body)
	b := hex.EncodeToString(md.Sum(nil))
	return string(a)+string(b)
}
